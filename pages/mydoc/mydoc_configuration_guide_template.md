---
title: Configuration Guide Template
keywords: template, MapperName, configuration
last_updated: May 20, 2019
summary: "Modify the Mapper Name placeholder"
sidebar: mydoc_sidebar
permalink: mydoc_configuration_guide_template.html
folder: mydoc
---

To use this template, you must modify the \<MapperName> placeholder. In the code references, replace \<MapperName> with the mapper name in lower case (for example, newmapper). In all other references, capitalize the mapper name as the proper noun (for example, New Mapper).

You must also review the following sections and update as needed so that it is specific to your mapper:

- **Add the \<MapperName> Mapper to the `docker-compose.yml` File -** This will be provided by the CC Mapper team when the RC build is complete.
- **Copy the \<MapperName> Mapper Resources Folder from Artifactory to the Environment -** This is provided by the team who builds the mapper. You need to update the list of artifacts that are included in the `resources.zip` file.
- **Update the Parameters in the `mapper-config.json` File -** This is provided by the team who builds the mapper. You need to update the code block to include the correct configuration parameters and default/placeholder values and the table to include the correct description of those configuration parameters.

Those are all the sections that need to be updated at this time. The other sections are static and can be used as is, as long as no changes are made to the configuration process. That being said, your team needs to work closely with the IT/Ops team who deploy and configure your mappers to ensure that they have the information that they need to successfully deploy and configure your mapper.

## Assumptions

Setting up the CrossCore environment and uploading the mapper is not covered in this document. This document will specifically outline the \<MapperName> mapper and the configuration required for setting up a working call through the CrossCore environment.

## Configuration to be Performed by the IT Team

The following steps are required to modify a CrossCore test environment to include the \<MapperName> mapper.

If configuring the mappers for UAT, remember that \<MapperName> UAT is a customer-facing environment.

\<MapperName> mapper configuration requires updates to the following files:

| File | Description |
| ---- |------------ |
| `docker-compose.yml` | The docker compose file. |
| `mapper-config.json` | JSON file containing mapper configuration key-value pairs. |

You may see some scripts included in the mapper configuration artifact package. The scripts are used for internal testing. You must have root access to run the scripts successfully. Because most delivery and IT teams do not have root access permissions, the instructions on this page describe how to configure the mapper using the `mapper-config-cli` tool. These are the recommended instructions from the CrossCore team.

The configuration of these files can be broken down into these steps:

1. Add the \<MapperName> mapper to the `docker-compose.yml` file.
2. Copy the \<MapperName> mapper Resources folder from Artifactory to the environment.
3. Update the parameters in the `mapper-config.json` file.
4. Seed the mapper configuration to ZooKeeper.
5. Seed the backing application version to ZooKeeper.

The following process explains how to configure the files above and execute the mapper configuration.

### Add the \<MapperName> Mapper to the `docker-compose.yml` File

1. To configure the \<MapperName> mapper, edit the `docker-compose.yml` file with a text editor (for example vi) and add the following \<MapperName> mapper section (the values used are examples)
```

    mapper-<Mapper-Name>:
    container_name: 'mapper-<MapperName>'
    image: 'redbox-docker-release-local.eda-prod-hud03.uk.experian.local:2222/mapper-<MapperName>:version'
    environment:
        DW_ZOOKEEPER_CONNECT_STRING: 'zookeeper.experian.local:2181' # change to host:port of zookeeper
        DW_ZOOKEEPER_KEY_STORE_PASSWORD: 'secret123' # change to password of keystore-zookeeper.jks
        DW_DISCOVERY_HOST: 'mapper.experian.local' # change to hostname of server running this container
        DW_DISCOVERY_PORT: '8443'
    volumes:
        - '/data/config/map/keystore-zookeeper.jks:/opt/crosscore/keys/keystore-zookeeper.jks'
    ports:
        - '<PORT>:8443'
```

2. Each mapper within Docker listens on port 8443 by default, but if several mappers are running on the Docker host, they can't all be exposed on the same internal port. You must expose a port on the mapper server for each mapper deployed. Configure each mapper to use a different port and set the DW_DISCOVERY_HOST value and variable to match that port.
3. Save changes to the file.
4. Run the following command to install the \<MapperName> mapper.

    `sudo docker-compose -f <path to config file>/docker-compose.yml up -d`

5. Replace `docker-compose.yml` with the actual name of your YAML file.

### Copy the \<MapperName> Mapper Resources Folder from Artifactory to the Environment

Each CrossCore backing application requires a number of configuration resources. These resources are stored in **Artifactory** and need to be copied to the Mapper server where the `mapper-config-cli` tool is installed. The \<MapperName> mapper resources directory has the following structure.

```
| -mapper-config.json
```

You may see some scripts included in the mapper configuration artifact package. The scripts are used for internal testing. You must have root access to run the scripts successfully. Because most delivery and IT teams do not have root access permissions, the instructions on this page describe how to configure the mapper using the `mapper-config-cli` tool. These are the recommended instructions from the CrossCore team.

